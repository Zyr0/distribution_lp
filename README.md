# distribution_lp

This is a project has presented in LP (Laboratório de programação) with 100% weight on the final
note of the UC (Unidade Curricular) read `ac_project.pdf`

## Notes of the presentation

After evaluation the teacher pointed the following erros/bugs with the code

- Problem relesing memory beffore exit;
- When reading file, memory is being replaced;
- When memory is not allocated or reallocated properly the programm should not exit insted show error message;
- Its letting users edit the order after this has been expedited;
- Its letting users delete order that has been expedited;
- Error on the listings with the printing;

Personal note: I don't agree with the second point.

## Objectives

This project is an integrating element of the knowledge acquired in the UC's of LP and FP (Fundamentos de programação)

### Specific objectives

- Specify and coordinate a small group project;
- Understand and master theoretical and practical knowledge about algorithms and programming in C;
- Acquire skills with a view to solving problems, namely through the research and autonomous use of
  external content and tools;
- Use the development of software development project for small / medium dimension as an essential
  element of the individual learning process;

## Common functionalities

The section serves to describe a set of functionalities implemented.

- Management of users \
  Create, Edit, Remove (set as inactive, information is not deleted)
- Management of orders \
  - Items
  - Order type
  - Total km \
    Create, Edit, Remove, List
- Management of Prices \
  - Order type
  - Price km
  - Factor postal code \
    Create, Edit, List
- Management of Invoices \
  Create, List
- Data persistence \
  Store and load from file

> The Director is the one that can reactive users and create/edit prices.

## Group listings

Implement 5 listings, this must be of interest to the user or the director. The main objective is
to analise data.

## Helpers

#### **order_type**

- Regular (Marked by the user cannot be Urgent)
- Urgent (Marked by the user cannot be Regular)
- Bulky (If total volume of items is bigger than x `consts.h`)
- Fragile (If at least une item is marked has fragil)
- Heavy (If total weight of items is bigger than x `consts.h`)

#### Calculate order price:

```math
price = (\sum order\_type + km * price\_km) * factor\_pc
```

#### Documentation

Usage of Doxygen to document the entire project

#### Statement

The statement can be found in the file named `ac_project.pdf`
