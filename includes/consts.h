/**
 * File:   consts.h
 *
 * Created on December 12, 2019, 10:13 AM
 *
 * This is a header file with de default sizes for the strings, this is needed in case of the
 * string
 */

#ifndef CONSTS_H
#define CONSTS_H

/**
 * @brief define a string with 25 characters
 */
#define SHORT_STRING 25

/**
 * @brief define a string with 50 characters
 */
#define NORMAL_STRING 50

/**
 * @brief define a string with 50 characters
 */
#define LONG_STRING 550

/**
 * @brief the initial size of the client list
 */
#define INIT_SIZE 10

/**
 * @brief to define a order as regular
 */
#define REGULAR 0

/**
 * @brief to define a order as urgent
 */
#define URGENT 1

/**
 * @brief to define a order as bulky
 */
#define BULKY 2

/**
 * @brief to define a order as fragile
 */
#define FRAGILE 3

/**
 * @brief to define a order as heavy
 */
#define HEAVY 4

/**
 * @brief to define the max volume in m^3 before considered BULKY
 */
#define MAX_VOLUME 0.125

/**
 * @brief to define the max weight in kg before considered HEAVY
 */
#define MAX_WEIGHT 20

/**
 * @brief to define the size of the price matrix
 */
#define MAX_SIZE_POSTAL_CODE 9

/**
 * @brief to define the max price for the order type
 */
#define MAX_PRICE_ORDER_TYPE 99.99

/**
 * @brief to define the max price for the factor postal code
 */
#define MAX_FACTOR_POSTAL_CODE 99.99

/**
 * @brief to define the max quantity of an item
 */
#define MAX_ITEM_QUANTITY 5000

/**
 * @brief to define the max weight of an item
 */
#define MAX_ITEM_WEIGHT 500

/**
 * @brief to define the max length of an item
 */
#define MAX_ITEM_LENGTH 3000

/**
 * @brief to define the max width of an item
 */
#define MAX_ITEM_WIDTH 3000

/**
 * @brief to define the max height of an item
 */
#define MAX_ITEM_HEIGHT 3000

/**
 * @brief to define the max price per km
 */
#define MAX_PRICE_KM 99.99

/**
 * @brief to define the max km an order can have
 */
#define MAX_ORDER_KM 100000000.0

/**
 * @brief Message to inform the user to press the key
 */
#define MSG_WAIT "Prima [Enter] para continuar!!"

/**
 * @brief Message to inform the user in case the signed short doesn't respect the range decided
 */
#define MSG_SIGNED_SHORT_INVALID_RANGE "O numero que introduziu e invalido. Introduza um valor entre %hd e %hd.\n"

/**
 * @brief Message to inform the user in case the unsigned short doesn't respect the range decided
 */
#define MSG_UNSIGNED_SHORT_INVALID_RANGE "O numero que introduziu e invalido. Introduza um valor entre %hu e %hu.\n"

/**
 * @brief Message to inform the user in case the signed int doesn't respect the range decided
 */
#define MSG_SIGNED_INT_INVALID_RANGE "O numero que introduziu e invalido. Introduza um valor entre %d e %d.\n"

/**
 * @brief Message to inform the user in case the unsigned int doesn't respect the range decided
 */
#define MSG_UNSIGNED_INT_INVALID_RANGE "O numero que introduziu e invalido. Introduza um valor entre %u e %u.\n"

/**
 * @brief Message to inform the user in case the signed long integer doesn't respect the range decided
 */
#define MSG_SIGNED_LONG_INVALID_RANGE "O numero que introduziu e invalido. Introduza um valor entre %ld e %ld.\n"

/**
 * @brief Message to inform the user in case the unsigned long integer doesn't respect the range decided
 */
#define MSG_UNSIGNED_LONG_INVALID_RANGE "O numero que introduziu e invalido. Introduza um valor entre %lu e %lu.\n"

/**
 * @brief Message to inform the user in case the float or double doesn't respect the range decided
 */
#define MSG_FLOAT_DOUBLE_INVALID_RANGE "\nO numero que introduziu e invalido. Introduza um valor entre %.3f e %.3f.\n"

/**
 * @brief Message to inform the user in case the integer or float is invalid
 */
#define MSG_INVALID_NUMBER "\nO valor que introduziu não e um numero valido.\n"

/**
 * @brief Message to inform the table need to be created to proceed
 */
#define MSG_TABLE_NEDDED "\nNecessário criar tabela para prosseguir\n"

#endif	/* CONSTS_H */

