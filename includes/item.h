/**
 * File:   item.h
 *
 * Created on December 12, 2019, 10:04 AM
 *
 * This file contains the definition of item and all of the function related to the item
 */

#ifndef ITEM_H
#define ITEM_H

#include "consts.h"

typedef struct item {
  char name[NORMAL_STRING + 1];
  unsigned int quantity;
  float weight, volume;
  unsigned short int fragile;
} ITEM;

typedef struct list_item {
  ITEM *elements;
  size_t length;
  size_t size;
} LIST_ITEM;

void init_list_item(LIST_ITEM *, const size_t);

size_t item_create(LIST_ITEM *, const ITEM);

size_t item_remove(LIST_ITEM *, const size_t);

long int item_exists(const LIST_ITEM, const char *);

void list_items(const LIST_ITEM);

void list_item(const ITEM);

void change_item_name(char *);

void change_item_quantity(unsigned int *);

void change_item_fragile(unsigned short int *);

void change_item_weight(float *);

void change_item_volume(float *);

ITEM get_item(const char *);

float get_total_weight(const LIST_ITEM);

float get_total_volume(const LIST_ITEM);

long int get_total_fragile(const LIST_ITEM);

#endif	/* ITEM_H */

