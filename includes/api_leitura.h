/*
 * File:   api_leitura.h
 *
 * Created on December 16, 2019, 2:24 PM
 *
 * This file is based on API_Leitura.h API_Utils.h provided in FP
 * The main difference is on readString that wasn't following the ANSI C as required in this project
 */

#ifndef API_LEITURA_H
#define API_LEITURA_H

void cleanInputBuffer();

void read_signed_short(
    short *const value,
    const short minValue,
    const short maxValue,
    char const* const message
    );

void read_unsigned_short(
    unsigned short *const value,
    const unsigned short minValue,
    const unsigned short maxValue,
    char const* const message
    );

void read_signed_int(
    int *const value,
    const int minValue,
    const int maxValue,
    char const* const message
    );

void read_unsigned_int(
    unsigned int *const value,
    const unsigned int minValue,
    const unsigned int maxValue,
    char const* const message
    );

void read_signed_long(
    long *const value,
    const long minValue,
    const long maxValue,
    char const* const message
    );

void read_unsigned_long(
    unsigned long *const value,
    const unsigned long minValue,
    const unsigned long maxValue,
    char const* const message
    );

void read_float(
    float *const value,
    const float minValue,
    const float maxValue,
    char const* const message
    );

void read_double(
    double *const value,
    const double minValue,
    const double maxValue,
    char const* const message
    );

void read_char(
    char *const value,
    char const* const message
    );

int read_string(
    char *const value,
    const unsigned int size,
    char const* const message
    );

#endif /* API_LEITURA_H */

