/**
 * File:   price.h
 *
 * Created on 9 de janeiro de 2020, 09:24
 */

#ifndef PRICE_H
#define PRICE_H

#include "consts.h"
#include "order.h"

typedef struct price_table {
    float price_postal_code[MAX_SIZE_POSTAL_CODE][MAX_SIZE_POSTAL_CODE],
        regular, urgent, bulky, fragile, heavy, price_km;
    unsigned short int cod_table_status, order_type_table_status;
} PRICE_TABLE;

void init_price_table(PRICE_TABLE *);

unsigned short int is_price_defined(const PRICE_TABLE table);

void create_postal_code_prices(PRICE_TABLE *);

void create_order_type_prices(PRICE_TABLE *);

void edit_postal_code_prices(PRICE_TABLE *);

void edit_order_type_prices(PRICE_TABLE *, const unsigned short int);

void show_postal_code_prices(const PRICE_TABLE);

void show_order_type_prices(const PRICE_TABLE);

void define_km_price(PRICE_TABLE *);

void show_km_price(const PRICE_TABLE);

void calc_price(ORDER *, PRICE_TABLE *);

#endif /* PRICE_H */

