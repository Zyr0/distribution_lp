/**
 * File:   listings.h
 *
 * Created on January 10, 2020, 7:08 PM
 */

#ifndef LISTINGS_H
#define LISTINGS_H

#include "order.h"
#include "client.h"

typedef struct tuple {
  ORDER order;
  CLIENT client;
} TUPLE;

unsigned long int order_expedited_in_month(const LIST_CLIENT, const unsigned short int);

TUPLE order_light_in_month(const LIST_CLIENT, const unsigned short int);

TUPLE order_expensive_in_month(const LIST_CLIENT, const unsigned short int);

CLIENT most_orders_in_month(const LIST_CLIENT, const unsigned short int);

CLIENT most_spent_in_month(const LIST_CLIENT, const unsigned short int);

void print_tuple(const TUPLE);

#endif /* LISTINGS_H */

