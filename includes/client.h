/**
 * File:   client.h
 *
 * Created on December 12, 2019, 12:31 PM
 *
 * This file contains the client definitions
 */

#ifndef CLIENT_H
#define CLIENT_H

#include "consts.h"
#include "address.h"
#include "order.h"

typedef struct client {
  unsigned long int nif, cc;
  char name[NORMAL_STRING + 1];
  ADDRESS address;
  LIST_ORDER orders;
  unsigned short int active;
  unsigned short int has_orders;
} CLIENT;

typedef struct list_client {
  CLIENT *elements;
  size_t length;
  size_t size;
} LIST_CLIENT;

void init_list_client(LIST_CLIENT *, size_t);

size_t client_create(LIST_CLIENT *, CLIENT);

void client_resize(LIST_CLIENT *);

void list_clients(const LIST_CLIENT);

void print_profile(const CLIENT client);

CLIENT read_client(const unsigned long int);

long int client_exists(const LIST_CLIENT, const unsigned long int);

int client_is_active(const CLIENT *, const unsigned long int);

void change_client_name(char *);

void change_cc(unsigned long int *);

void change_active_state(unsigned short int *);

#endif /* CLIENT_H */

