/**
 * File:   order_type.h
 *
 * Created on December 12, 2019, 11:31 AM
 *
 * This file contains the order_types
 */

#ifndef ORDER_TYPE_H
#define ORDER_TYPE_H

#include "consts.h"

typedef struct order_type {
  unsigned short int type;
} ORDER_TYPE;

typedef struct list_order_type {
  ORDER_TYPE *elements;
  size_t length;
  size_t size;
} LIST_ORDER_TYPE;

void init_list_order_type(LIST_ORDER_TYPE *, const size_t);

size_t order_type_create(LIST_ORDER_TYPE *, const ORDER_TYPE);

void list_order_types(const LIST_ORDER_TYPE);

void list_order_type();

#endif /* ORDER_TYPE_H */
