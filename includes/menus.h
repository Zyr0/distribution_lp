/*
 * File:   menus.h
 *
 * Created on December 19, 2019, 9:56 AM
 */

#ifndef MENUS_H
#define MENUS_H

#include "client.h"
#include "order.h"
#include "item.h"
#include "price.h"

void menu_client(LIST_CLIENT *, PRICE_TABLE *);

void menu_director(LIST_CLIENT *, PRICE_TABLE *);

void menu_listings(const LIST_CLIENT);

void menu_manage_clients(LIST_CLIENT *);

void menu_manage_orders(LIST_CLIENT *);

void menu_order_client_manage(CLIENT *, PRICE_TABLE *);

void menu_order_client_edit(LIST_ORDER *, const long int, PRICE_TABLE *);

void menu_item_manage(LIST_ITEM *);

void menu_item_edit(ITEM *);

void menu_client_manage_acount(CLIENT *);

void menu_client_edit_profile(CLIENT *);

void menu_address_edit(ADDRESS *);

void menu_order_type_price(PRICE_TABLE *);

void menu_prices(PRICE_TABLE *);

void menu_show_prices(PRICE_TABLE *);

void menu_define_prices(PRICE_TABLE *);

#endif /* MENUS_H */

