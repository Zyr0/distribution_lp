/**
 * File:   order.h
 *
 * Created on December 12, 2019, 10:30 AM
 *
 * This file contains the orders definitions for the orders
 */

#ifndef ORDER_H
#define ORDER_H

#include "item.h"
#include "address.h"
#include "order_type.h"
#include "date.h"

typedef struct order {
  unsigned long int id;
  DATE date_init, date_ex;
  LIST_ITEM items;
  ADDRESS start, end;
  LIST_ORDER_TYPE order_type;
  double km;
  unsigned short int state;
  float price;
} ORDER;

typedef struct list_order {
  ORDER *elements;
  size_t length;
  size_t size;
  unsigned long int id_count;
} LIST_ORDER;

void init_list_order(LIST_ORDER *, size_t);

size_t order_create(LIST_ORDER *, ORDER);

size_t order_remove(LIST_ORDER *, const size_t);

long int order_exists(const LIST_ORDER, const unsigned long int);

unsigned int is_expedited(const ORDER *, long int);

void list_orders(const LIST_ORDER);

void list_order(const ORDER);

ORDER get_order(LIST_ITEM *, const ADDRESS, const unsigned long int);

void change_order_type_list(const LIST_ITEM *, LIST_ORDER_TYPE *);

void change_start_address(ADDRESS *, const ADDRESS);

void change_order_state(ORDER *);

void change_order_km(double *);

unsigned short int order_is_urgent();

#endif /* ORDER_H */
