/**
 * File:   invoice.h
 *
 * Created on January 10, 2020, 2:07 PM
 */

#ifndef INVOICE_H
#define INVOICE_H

#include "order.h"

void print_invoice(const LIST_ORDER orders, unsigned short int);

#endif /* INVOICE_H */

