/**
 * File:   files.h
 *
 * Created on January 11, 2020, 10:13 AM
 */

#ifndef FILES_H
#define FILES_H

#include "client.h"
#include "price.h"

void save_to_file(LIST_CLIENT *, PRICE_TABLE *);

void read_from_file(LIST_CLIENT *, PRICE_TABLE *, size_t);

void save_price_table(PRICE_TABLE *, FILE *);

void save_clients(LIST_CLIENT *clients, FILE *file);

void save_orders(LIST_ORDER *orders, FILE *file);

void save_items(LIST_ITEM *items, FILE *file);

void save_order_type(LIST_ORDER_TYPE *, FILE *);

void read_price_table(PRICE_TABLE *table, FILE *file);

void read_clients(LIST_CLIENT *clients, FILE *file, size_t);

void read_orders(LIST_ORDER *orders, FILE *file);

void read_items(LIST_ITEM *items, FILE *file);

void read_order_type(LIST_ORDER_TYPE *, FILE *);

#endif /* FILES_H */

