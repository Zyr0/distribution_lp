/**
 * File:   date.h
 *
 * Created on December 12, 2019, 12:04 PM
 *
 * This is de date
 */

#ifndef DATE_H
#define DATE_H

typedef struct date {
  unsigned short int day,
                 month,
                 year;
  int hour, min, sec;
} DATE;

DATE get_current_date();

void print_date(const DATE date);

#endif /* DATE_H */

