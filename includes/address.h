/**
 * File:   address.h
 *
 * Created on December 12, 2019, 10:42 AM
 *
 * This file contains the address definitions
 */

#ifndef ADDRESS_H
#define ADDRESS_H

#include "consts.h"

typedef struct address {
  char street[NORMAL_STRING + 1],
       local[NORMAL_STRING + 1],
       district[NORMAL_STRING + 1];
  unsigned short int zip_4, zip_3;
} ADDRESS;

ADDRESS get_address();

void print_address(const ADDRESS address);

void change_street(char *);

void change_local(char *);

void change_district(char *);

void change_zip(unsigned short int *, unsigned short int *);

void change_address(ADDRESS *);

#endif /* ADDRESS_H */

