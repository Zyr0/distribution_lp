/*
 * File:   address.c
 *
 * Created on December 18, 2019, 6:30 PM
 */

#include<stdio.h>
#include<stdlib.h>
#include "../includes/api_leitura.h"
#include "../includes/consts.h"
#include "../includes/address.h"

/**
 * @brief method that asks for the information of an address
 * @return the address imputed for the user
 */
ADDRESS get_address() {
  ADDRESS address;
  printf("\nIntroduza morada\n");
  change_street(address.street);
  change_local(address.local);
  change_district(address.district);
  change_zip(&address.zip_4, &address.zip_3);
  return address;
}

/**
 * @brief a method that prints a certain address
 * @param address the address to be printed
 */
void print_address(const ADDRESS address) {
  printf("Morada\n");
  printf("Rua: %s\n", address.street);
  printf("Localidade: %s\n", address.local);
  printf("Distrito: %s\n", address.district);
  printf("Código postal: %hu-%hu\n", address.zip_4, address.zip_3);
}

/**
 * @brief a function that asks for a street to be imputed
 * @param s the pointer where the street will be stored
 */
void change_street(char *s) {
  read_string(s, NORMAL_STRING + 1, "Introduza nome da rua: " );
}

/**
 * @brief a function that asks for a local to be imputed
 * @param s the pointer where the local will be stored
 */
void change_local(char *s) {
  read_string(s, NORMAL_STRING + 1, "Introduza a localidade: " );
}

/**
 * @brief a function that asks for a district to be imputed
 * @param s the pointer where the district will be stored
 */
void change_district(char *s) {
  read_string(s, NORMAL_STRING + 1, "Introduza o distrito: ");
}

/**
 * @brief a function that asks for the zip code
 * @param zip_4 the pointer where the first 4 digits of the zip code will be stored
 * @param zip_3 the pointer where the last 3 digits of the zip code will be stored
 */
void change_zip(unsigned short int *zip_4, unsigned short int *zip_3) {
  read_unsigned_short(zip_4, 1000, 9999, "Introduza os primeiros 4 dígitos do código postal: ");
  read_unsigned_short(zip_3, 100, 999, "Introduza os últimos 3 dígitos do código postal: ");
}

/**
 * @brief a method that changes the entire address
 * @param address a pointer where the address will be stored
 */
void change_address(ADDRESS *address) {
  *address = get_address();
}
