/**
 * File:   main.c
 *
 * Created on December 12, 2019, 9:57 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include "../includes/api_leitura.h"
#include "../includes/client.h"
#include "../includes/menus.h"
#include "../includes/consts.h"
#include "../includes/price.h"
#include "../includes/files.h"

/**
 * TODO: guardar dados
 * TODO: carregar dados
 */
int main() {
  unsigned short int op;
  LIST_CLIENT clients;
  PRICE_TABLE price_table;

  init_list_client(&clients, INIT_SIZE);
  init_price_table(&price_table);

  do {
    printf("\n###################\n");
    printf("#\tMENU\t  #\n");
    printf("###################\n");
    printf("1 - Utilizador\n2 - Diretor\n3 - Carregar Dados\n4 - Guardar Dados\n0 - Sair\n");
    read_unsigned_short(&op, 0, 4, "Introduza uma opção: ");

    switch (op) {
      case 0: printf("A sair ...\n"); break;
      case 1:
        if (is_price_defined(price_table) == 1) {
          putchar('\n');
          menu_client(&clients, &price_table);
        } else {
          printf("\nPreços por definir\n");
        }
        break;
      case 2:
        menu_director(&clients, &price_table);
        break;
      case 3:
        read_from_file(&clients, &price_table, clients.length);
        break;
      case 4:
        if (is_price_defined(price_table) == 1 && clients.size > 0) {
          save_to_file(&clients, &price_table);
        } else {
          printf("\nPreços ou clients por definir\n");
        }
        break;
    }
  } while (op != 0);

  free(clients.elements);
  return (EXIT_SUCCESS);
}

