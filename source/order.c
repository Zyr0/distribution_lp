/*
 * File:   order.c
 *
 * Created on December 21, 2019, 6:40 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../includes/api_leitura.h"
#include "../includes/consts.h"
#include "../includes/order.h"
#include "../includes/address.h"
#include "../includes/date.h"
#include "../includes/order_type.h"

/**
 * @brief initialize the list of orders @warning if the allocation fails the program exits
 * with code -1
 * @param list receives a LIST_ORDER
 * @param init_size the initial size of the list
 */
void init_list_order(LIST_ORDER *list, size_t init_size) {
  list->elements = (ORDER *) malloc(init_size * sizeof(ORDER));
  if (list->elements == NULL) {
    fprintf(stderr, "Erro ao alocar memória\n");
    exit(-1);
  }
  list->length = init_size;
  list->size = 0;
  list->id_count = 0;
}

/**
 * @brief a method adds a new order to the list @warning if the list cannot be
 * reallocated prints a error message and exits with code -1
 * @param list the list of orders
 * @param order the order to add to the list
 * @return the current size of the list
 */
size_t order_create(LIST_ORDER *list, ORDER order) {
  if (list->size == list->length) {
    list->length += 1;
    list->elements = (ORDER *) realloc(list->elements, list->length * sizeof(ORDER));
    if (list->elements == NULL) {
      fprintf(stderr, "Erro ao realocar memória\n");
      exit(-1);
    }
  }
  list->id_count++;
  list->elements[list->size++] = order;
  return list->size;
}

/**
 * @brief a method that removes a item from the list @warning if the list cannot be
 * reallocated prints a error message and exits with code -1
 * @param list the list of orders
 * @param pos the position to remove
 * @return the current size of the list
 */
size_t order_remove(LIST_ORDER *list, const size_t pos) {
  list->elements[pos] = list->elements[list->size];
  list->size--;
  list->length -= 1;
  list->elements = (ORDER *) realloc(list->elements, list->length * sizeof(ORDER));
  if (list->elements == NULL) {
    fprintf(stderr, "Erro ao realocar memória\n");
    exit(-1);
  }
  return list->size;
}

/**
 * @brief a methods that checks if a certain order exists
 * @param list a list of orders
 * @param id the id of the order
 */
long int order_exists(const LIST_ORDER list, const unsigned long int id) {
  long int i, size = list.size;
  for (i = 0; i < size; i++) {
    if (list.elements[i].id == id)
      return i;
  }
  return -1;
}

/**
 * @brief a method that checks if a order has been expedited
 * @param order the list of orders to check
 * @param pos the position to check in the list
 * @return 1 if the order is expedited and 0 if not
 */
unsigned int is_expedited(const ORDER *order, long int pos) {
  return order[pos].state == 1;
}

/**
 * @brief prints the details of a certain list
 * @param list the list to print
 */
void list_orders(const LIST_ORDER list) {
  int i;
  long size = list.size;
  for(i = 0; i < size; i++) {
    list_order(list.elements[i]);
  }
}

/**
 * @brief prints the details of a order
 * @param order the order to print
 */
void list_order(const ORDER order) {
  printf("\n\nEncomenda ID: %lu\n", order.id);
  printf("Data criada: ");
  print_date(order.date_init);
  printf("Data expedição: ");
  if (order.state == 1)
    print_date(order.date_ex);
  else
    printf("Por expedir\n");
  printf("\nOrigem ");
  print_address(order.start);
  printf("\nDestino ");
  print_address(order.end);
  printf("\nDistancia de %.2f km\n", order.km);
  putchar('\n');
  printf("Peso: %f\n", get_total_weight(order.items));
  printf("Volume: %f\n", get_total_volume(order.items));
  putchar('\n');
  list_order_types(order.order_type);
  printf("\nPreço de %.2f €\n", order.price);
}

/**
 * @brief a method that asks for the order details @warning the price is not calculated where
 * @param list the list of items to be added
 * @param address the address of the client
 * @param id the id of the order
 */
ORDER get_order(LIST_ITEM *list, const ADDRESS address, const unsigned long int id) {
  ORDER order;
  order.id = id;
  order.items = *list;
  order.state = 0;
  order.date_init = get_current_date();
  change_start_address(&order.start, address);
  printf("\nMorada final");
  change_address(&order.end);
  change_order_km(&order.km);
  init_list_order_type(&order.order_type, 1);
  change_order_type_list(list, &order.order_type);
  return order;
}

/**
 * @brief a method that changes the order types of the order
 * @param item_list the list of items in the order
 * @param order_type_list the order type list to update
 */
void change_order_type_list(const LIST_ITEM *item_list, LIST_ORDER_TYPE *order_type_list) {
  ORDER_TYPE order_type;
  if (get_total_volume(*item_list) > MAX_VOLUME) {
    order_type.type = BULKY;
    order_type_create(order_type_list, order_type);
  }

  if (get_total_weight(*item_list) > MAX_WEIGHT) {
    order_type.type = HEAVY;
    order_type_create(order_type_list, order_type);
  }

  if (get_total_fragile(*item_list) > 0) {
    order_type.type = FRAGILE;
    order_type_create(order_type_list, order_type);
  }

  if (order_is_urgent() == 1) {
    order_type.type = URGENT;
  } else {
    order_type.type = REGULAR;
  }
  order_type_create(order_type_list, order_type);
}

/**
 * @brief a method that changes the start address of an order
 * @param address a pointer to where the address will be stored
 * @param start the origin address
 */
void change_start_address(ADDRESS *address, const ADDRESS start) {
  unsigned short int op;
  printf("Usar a murada cliente como inicial?\n1 - Sim\n0 - Não\n");
  read_unsigned_short(&op, 0, 1, "Introduza uma opção: ");
  if (op == 1)
    *address = start;
  else
    change_address(address);
}

/**
 * @brief a method that changes the state of the order
 * @param order the order that will be modified
 */
void change_order_state(ORDER *order) {
  unsigned short int op;
  printf("Marcar encomenda como expedida?\n1 - Sim\n0 - Não\n");
  read_unsigned_short(&op, 0, 1, "Introduza uma opção: ");
  if (op == 1) {
    (*order).date_ex = get_current_date();
    (*order).state = 1;
  } else {
    (*order).state = 0;
  }
}

/**
 * @brief a function that asks for the km
 * @param km the pointer where the km will be stored
 */
void change_order_km(double *km) {
  read_double(km, 0.1, MAX_ORDER_KM, "\nIntroduza os km: ");
}

/**
 * @brief a function that asks if the order is urgent
 * @return 1 if urgent 0 if not
 */
unsigned short int order_is_urgent() {
  unsigned short int op;
  printf("\nMarcar encomenda como urgente?\n1 - Sim\n0 - Não\n");
  read_unsigned_short(&op, 0, 1, "Introduza uma opção: ");
  return op;
}
