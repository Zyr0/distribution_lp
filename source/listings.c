/**
 * File:   listings.c
 *
 * Created on January 10, 2020, 7:08 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include "../includes/api_leitura.h"
#include "../includes/listings.h"
#include "../includes/client.h"
#include "../includes/order.h"
#include "../includes/item.h"

/**
 * @brief a method that counts how many orders where expedited in the month
 * @param clients the clients with the orders
 * @param month the month to check
 * @returns the count of orders expedited in the month
 */
 unsigned long int order_expedited_in_month(const LIST_CLIENT clients, const unsigned short int month) {
  unsigned long int count = 0;
  size_t i, j, size_orders, size_clients = clients.size;
  LIST_ORDER orders;

  for (i = 0; i < size_clients; i++) {
    orders = clients.elements[i].orders;
    size_orders = orders.size;
    for (j = 0; j < size_orders; j++) {
      if (is_expedited(orders.elements, j) == 1 && orders.elements[j].date_ex.month == month) {
        count++;
      }
    }
  }
  return count;
}

/**
 * @brief a methods that returns the lightest order of the month (not expedited)
 * @param clients the clients with the orders
 * @param month the month to check
 * @returns a tuple containing the lightest order and client
 */
TUPLE order_light_in_month(const LIST_CLIENT clients, const unsigned short int month) {
  size_t i, j, size_orders, size_clients = clients.size;
  float weigth, prev;
  unsigned short int flag = 1;
  LIST_ORDER orders;
  TUPLE tuple;

  for (i = 0; i < size_clients; i++) {
    orders = clients.elements[i].orders;
    size_orders = orders.size;
    for (j = 0; j < size_orders; j++) {
      if (orders.elements[j].date_init.month == month) {
        weigth = get_total_weight(orders.elements[j].items);

        if (flag == 1) {
          prev = weigth;
          tuple.order = orders.elements[j];
          tuple.client = clients.elements[i];
          flag = 0;
        }

        if (weigth < prev) {
          prev = weigth;
          tuple.order = orders.elements[j];
          tuple.client = clients.elements[i];
        }
      }
    }
  }
  return tuple;
}

/**
 * @brief a methods that returns the most expensive order of the month
 * @param clients the clients with the orders
 * @param month the month to check
 * @return a tuple containing the most expensive order and client
 */
TUPLE order_expensive_in_month(const LIST_CLIENT clients, const unsigned short int month) {
  size_t i, j, size_orders, size_clients = clients.size;
  float price, prev;
  unsigned short int flag = 1;
  LIST_ORDER orders;
  TUPLE tuple;

  for (i = 0; i < size_clients; i++) {
    orders = clients.elements[i].orders;
    size_orders = orders.size;
    for (j = 0; j < size_orders; j++) {
      if (orders.elements[j].date_init.month == month) {
        price = orders.elements[j].price;

        if (flag == 1) {
          prev = price;
          tuple.order = orders.elements[j];
          tuple.client = clients.elements[i];
          flag = 0;
        }

        if (price > prev) {
          prev = price;
          tuple.order = orders.elements[j];
          tuple.client = clients.elements[i];
        }
      }
    }
  }
  return tuple;
}

/**
 * @brief a methods that returns the client with most order of the month
 * @param clients the clients with the orders
 * @param month the month to check
 * @return client that has the most orders
 */
CLIENT most_orders_in_month(const LIST_CLIENT clients, const unsigned short int month) {
  size_t i, j, size_orders, size_clients = clients.size, count, prev;
  unsigned short int flag = 1;
  LIST_ORDER orders;
  CLIENT client;

  for (i = 0; i < size_clients; i++) {
    orders = clients.elements[i].orders;
    size_orders = orders.size;
    count = 0;
    for (j = 0; j < size_orders; j++) {
      if (orders.elements[j].date_init.month == month) {
        count++;
      }
    }

    if (flag == 1) {
      prev = count;
      client = clients.elements[i];
      flag = 0;
    }

    if (count > prev) {
      prev = count;
      client = clients.elements[i];
    }
  }
  return client;
}

/**
 * @brief a methods that returns the client with most
 * @param clients the clients with the orders
 * @param month the month to check
 * @return client that has the most orders
 */
CLIENT most_spent_in_month(const LIST_CLIENT clients, const unsigned short int month) {
  size_t i, j, size_orders, size_clients = clients.size, count, prev;
  unsigned short int flag = 1;
  LIST_ORDER orders;
  CLIENT client;

  for (i = 0; i < size_clients; i++) {
    orders = clients.elements[i].orders;
    size_orders = orders.size;
    count = 0;
    for (j = 0; j < size_orders; j++) {
      if (orders.elements[j].date_init.month == month) {
        count += orders.elements[j].price;
      }
    }

    if (flag == 1) {
      prev = count;
      client = clients.elements[i];
      flag = 0;
    }

    if (count > prev) {
      prev = count;
      client = clients.elements[i];
    }
  }
  return client;
}

/**
 * @brief a method that prints a tuple of client and order
 * @param tuple a structure that contains the order and client
 */
void print_tuple(const TUPLE tuple) {
  print_profile(tuple.client);
  list_order(tuple.order);
}
