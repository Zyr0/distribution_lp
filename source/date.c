/*
 * File:   date.c
 *
 * Created on January 7, 2020, 9:03 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../includes/api_leitura.h"
#include "../includes/consts.h"
#include "../includes/date.h"

/**
 * @brief a method that gets the current date with hours and seconds
 * @return the current date
 */
DATE get_current_date() {
  DATE date;
  time_t current_time = time(NULL);
  struct tm *tm = localtime(&current_time);
  date.day = tm->tm_mday;
  date.month = tm->tm_mon + 1;
  date.year = tm->tm_year + 1900;
  date.hour = tm->tm_hour;
  date.min = tm->tm_min;
  date.sec = tm->tm_sec;
  return date;
}

/**
 * @brief a method that prints the date formatted
 * @param date the date to print
 */
void print_date(const DATE date) {
  printf("%hu/%hu/%hu %d:%d:%d\n", date.day, date.month, date.year,
      date.hour, date.min, date.sec);
}
