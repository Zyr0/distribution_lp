/*
 * File:   api_leitura.c
 *
 * Created on December 16, 2019, 2:25 PM
 *
 * This file is based on API_Leitura.c and API_Utils.c provided in FP
 * The main difference is on readString that wasn't following the ANSI C as required in this project
 *
 */

#include <stdio.h>
#include <string.h>
#include "../includes/api_leitura.h"
#include "../includes/consts.h"

static const unsigned short int EXPECTED_ARGS = 1;
static const char CH_NEW_LINE = '\n';
static const char CH_END_STRING = '\0';

/**
 * @brief Cleans the keyboard buffer.
 * @warning If there is no information on the buffer the program will wait until there is information
 * on the buffer
 *
 * Example:
 * @code
 *  char car:
 *  car = getchar();
 *  clean_input_buffer();
 * @endcode
 */
void clean_input_buffer() {
    char ch;
    while ((ch = getchar()) != CH_NEW_LINE && ch != EOF);
}

/**
 * Sends a message to stdout
 * @param message  The message
 *
 * Example:
 * @code
 * display_message("Bears beat Battlestar Galactica!!!!");
 * @endcode
 */
void display_message(char const *const message) {
    printf("%s", message);
}

/**
 * ask for a signed short int from stdin
 * @param value pointer where the value will be stored
 */
void read_signed_short_from_stdin(short *const value) {
    while (scanf("%hd", value) != EXPECTED_ARGS) {
        printf(MSG_INVALID_NUMBER);
        clean_input_buffer();
    }
    clean_input_buffer();
}

/**
 * ask for a unsigned short int from stdin
 * @param value pointer where the value will be stored
 */
void read_unsigned_short_from_stdin(unsigned short *const value) {
    while (scanf("%hu", value) != EXPECTED_ARGS) {
        printf(MSG_INVALID_NUMBER);
        clean_input_buffer();
    }
    clean_input_buffer();
}

/**
 * Ask for a signed int from stdin
 * @param value pointer where the value will be stored
 */
void read_signed_int_from_stdin(int *const value) {
    while (scanf("%d", value) != EXPECTED_ARGS) {
        printf(MSG_INVALID_NUMBER);
        clean_input_buffer();
    }
    clean_input_buffer();
}

/**
 * Ask for a unsigned int from stdin
 * @param value pointer where the value will be stored
 */
void read_unsigned_int_from_stdin(unsigned int *const value) {
    while (scanf("%u", value) != EXPECTED_ARGS) {
        printf(MSG_INVALID_NUMBER);
        clean_input_buffer();
    }
    clean_input_buffer();
}

/**
 * Ask for a signed long from stdin
 * @param value pointer where the value will be stored
 */
void read_signed_long_from_stdin(long *const value) {
    while (scanf("%ld", value) != EXPECTED_ARGS) {
        printf(MSG_INVALID_NUMBER);
        clean_input_buffer();
    }
    clean_input_buffer();
}

/**
 * Ask for a unsigned long from stdin
 * @param value pointer where the value will be stored
 */
void read_unsigned_long_from_stdin(unsigned long *const value) {
    while (scanf("%lu", value) != EXPECTED_ARGS) {
        printf(MSG_INVALID_NUMBER);
        clean_input_buffer();
    }
    clean_input_buffer();
}

/**
 * Ask for a value from stdin
 * @param value pointer where the value will be stored
 */
void read_float_from_stdin(float *const value) {

    while (scanf("%f", value) != EXPECTED_ARGS) {
        printf(MSG_INVALID_NUMBER);
        clean_input_buffer();
    }
    clean_input_buffer();
}

/**
 * Ask for a value from stdin
 * @param value pointer where the value will be stored
 */
void read_double_from_stdin(double *const value) {

    while (scanf("%lf", value) != EXPECTED_ARGS) {
        printf(MSG_INVALID_NUMBER);
        clean_input_buffer();
    }
    clean_input_buffer();
}

/**
 * @brief Ask for a signed short integer
 * The number inserted by the user is between max and min values
 *
 * @param value pointer where the value will be stored
 * @param minValue the mim permitted value
 * @param maxValue the max permitted value
 * @param message message to show the user
 *
 * Example:
 * @code
 *  short x;
 *  read_signed_short(&x, -200, 200, "Enter a number between -200 e 200: ");
 * @endcode
 */
void read_signed_short(short *const value, const short minValue,
    const short maxValue, char const* const message) {

    display_message(message);
    read_signed_short_from_stdin(value);
    while (*value < minValue || *value > maxValue) {
        printf(MSG_SIGNED_SHORT_INVALID_RANGE, minValue, maxValue);
        display_message(message);
        read_signed_short_from_stdin(value);
    }
}

/**
 * @brief Ask for a unsigned short integer
 * The number inserted by the user is between max and min values
 *
 * @param value pointer where the value will be stored
 * @param minValue the mim permitted value
 * @param maxValue the max permitted value
 * @param message message to show the user
 *
 * Example:
 * @code
 *  unsigned short x;
 *  read_unsigned_short(&x, 0, 200, "Enter a number between 0 e 200: ");
 * @endcode
 */
void read_unsigned_short(unsigned short *const value, const unsigned short minValue,
        const unsigned short maxValue, char const* const message) {

    display_message(message);
    read_unsigned_short_from_stdin(value);
    while (*value < minValue || *value > maxValue) {
        printf(MSG_UNSIGNED_SHORT_INVALID_RANGE, minValue, maxValue);
        display_message(message);
        read_unsigned_short_from_stdin(value);
    }
}

/**
 * @brief Ask for a signed integer
 * The number inserted by the user is between max and min values
 *
 * @param value pointer where the value will be stored
 * @param minValue the mim permitted value
 * @param maxValue the max permitted value
 * @param message message to show the user
 *
 * Example:
 * @code
 *  int x;
 *  read_signed_int(&x, -200, 200, "Enter a number between -200 and 200: ");
 * @endcode
 */
void read_signed_int(int *const value, const int minValue,
        const int maxValue, char const* const message) {

    display_message(message);
    read_signed_int_from_stdin(value);
    while (*value < minValue || *value > maxValue) {
        printf(MSG_SIGNED_INT_INVALID_RANGE, minValue, maxValue);
        display_message(message);
        read_signed_int_from_stdin(value);
    }
}

/**
 * @brief Ask for a unsigned integer
 * The number inserted by the user is between max and min values
 *
 * @param value pointer where the value will be stored
 * @param minValue the mim permitted value
 * @param maxValue the max permitted value
 * @param message message to show the user
 *
 * Example:
 * @code
 *  unsigned int x;
 *  read_unsigned_int(&x, 0, 200, "Enter a number between 0 and 200: ");
 * @endcode
 */
void read_unsigned_int(unsigned int *const value, const unsigned int minValue,
        const unsigned int maxValue, char const* const message) {

    display_message(message);
    read_unsigned_int_from_stdin(value);
    while (*value < minValue || *value > maxValue) {
        printf(MSG_UNSIGNED_INT_INVALID_RANGE, minValue, maxValue);
        display_message(message);
        read_unsigned_int_from_stdin(value);
    }
}

/**
 * @brief Ask for a signed long integer
 * The number inserted by the user is between max and min values
 *
 * @param value pointer where the value will be stored
 * @param minValue the mim permitted value
 * @param maxValue the max permitted value
 * @param message message to show the user
 *
 * Example:
 * @code
 *  long x;
 *  read_singned_long(&x, -1000, 1000, "Enter a number between -1000 and 1000: ");
 * @endcode
 */
void read_signed_long(long *const value, const long minValue, const long maxValue,
        char const* const message) {

    display_message(message);
    read_signed_long_from_stdin(value);
    while (*value < minValue || *value > maxValue) {
        printf(MSG_SIGNED_LONG_INVALID_RANGE, minValue, maxValue);
        display_message(message);
        read_signed_long_from_stdin(value);
    }
}

/**
 * @brief Ask for a unsigned long integer
 * The number inserted by the user is between max and min values
 *
 * @param value pointer where the value will be stored
 * @param minValue the mim permitted value
 * @param maxValue the max permitted value
 * @param message message to show the user
 *
 * Example:
 * @code
 *  unsigned long x;
 *  read_unsigned_long(&x, 0, 1000, "Enter a number between 0 and 1000: ");
 * @endcode
 */
void read_unsigned_long(unsigned long *const value, const unsigned long minValue,
    const unsigned long maxValue, char const* const message) {

    display_message(message);
    read_unsigned_long_from_stdin(value);
    while (*value < minValue || *value > maxValue) {
        printf(MSG_UNSIGNED_LONG_INVALID_RANGE, minValue, maxValue);
        display_message(message);
        read_unsigned_long_from_stdin(value);
    }
}

/**
 * @brief Ask for a float
 * The number inserted by the user is between max and min values
 *
 * @param value pointer where the value will be stored
 * @param minValue the mim permitted value
 * @param maxValue the max permitted value
 * @param message message to show the user
 *
 * Example:
 * @code
 *  float x;
 *  read_float(&x, 10.0, 999.99, "Enter a number between 10.0 e 999.99: ");
 * @endcode
 */
void read_float(float *const value, const float minValue,
        const float maxValue, char const* const message) {

    display_message(message);
    read_float_from_stdin(value);
    while (*value < minValue || *value > maxValue) {
        printf(MSG_FLOAT_DOUBLE_INVALID_RANGE, minValue, maxValue);
        display_message(message);
        read_float_from_stdin(value);
    }
}

/**
 * @brief Ask for a double
 * The number inserted by the user is between max and min values
 *
 * @param value pointer where the value will be stored
 * @param minValue the mim permitted value
 * @param maxValue the max permitted value
 * @param message message to show the user
 *
 * Example:
 * @code
 *  double x;
 *  read_double(&x, 0.0, 999.99, "Enter a number between 0.0 e 999.99: ");
 * @endcode
 */
void read_double(double *const value, const double minValue,
        const double maxValue, char const* const message) {

    display_message(message);
    read_double_from_stdin(value);
    while (*value < minValue || *value > maxValue) {
        printf(MSG_FLOAT_DOUBLE_INVALID_RANGE, minValue, maxValue);
        display_message(message);
        read_double_from_stdin(value);
    }
}

/**
 * @brief Ask for a char.
 *
 * @param value pointer where the value will be stored
 * @param message message to show the user
 *
 * Example:
 * @code
 *  char ch;
 *  read_char(&ch, "Enter a character: ");
 * @endcode
 */
void read_char(char *const value, char const* const message) {
    display_message(message);
    *value = getchar();
    clean_input_buffer();
}

/**
 * @brief Ask for a string
 *
 * @param value pointer where the string will be stored
 * @param size maximum size of the string. @warning: The size of the
 * string most have a extra space for the '\0' character
 * @param message message to show the user
 *
 * @return 1 if reading goes well, 0 if there is an error.
 *
 * Example:
 * @code
 *  char name[100 + 1];
 *
 *  if (read_string (name, 100 + 1, "Enter your name: ") == 1)
 *          printf("The name is: %s", name);
 * @endcode
 */
int read_string(char * const value, const unsigned int size, char const* const message) {

    display_message(message);
    if (fgets(value, size, stdin) != NULL) {
        const unsigned int size = strlen(value) - 1;

        if (value[size] == CH_NEW_LINE) {
            value[size] = CH_END_STRING;
        } else {
            clean_input_buffer();
        }
        return 1;
    } else {
        return 0;
    }
}
