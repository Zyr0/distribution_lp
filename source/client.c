/**
 * File:   client.c
 *
 * Created on December 17, 2019, 2:03 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../includes/api_leitura.h"
#include "../includes/consts.h"
#include "../includes/client.h"
#include "../includes/address.h"

/**
 * @brief initialize the list of clients @warning if the allocation fails the program exits
 * with code -1
 * @param list receives a LIST_CLIENT
 * @param init_size the initial size of the list
 */
void init_list_client(LIST_CLIENT *list, size_t init_size) {
  list->elements = (CLIENT *) malloc(init_size * sizeof(CLIENT));
  if (list->elements == NULL) {
    fprintf(stderr, "Erro ao alocar memória\n");
    exit(-1);
  }
  list->length = init_size;
  list->size = 0;
}

/**
 * @brief a method adds a new client to the list @warning if the list cannot be
 * reallocated prints a error message and exits with code -1
 * @param list the list of clients
 * @param client the client to add to the list
 * @return the current size of the list
 */
size_t client_create(LIST_CLIENT *list, CLIENT client) {
  if (list->size == list->length) {
    list->length *= 2;
    list->elements = (CLIENT *) realloc(list->elements, list->length * sizeof(CLIENT));
    if (list->elements == NULL) {
      fprintf(stderr, "Erro ao realocar memória\n");
      exit(-1);
    }
  }
  list->elements[list->size++] = client;
  return list->size;
}

/**
 * @brief a method that resizes the client list to the size @warning if the list cannot be
 * reallocated prints a error message and exits with code -1
 * @param list the list of clients to resize
 */
void client_resize(LIST_CLIENT *list) {
  list->length = list->size;
  list->elements = (CLIENT *) realloc(list->elements, list->length * sizeof(CLIENT));
  if (list->elements == NULL) {
    fprintf(stderr, "Erro ao realocar memória\n");
    exit(-1);
  }
}

/**
 * @brief lists all of the clients
 * @param list the list of clients
 */
void list_clients(const LIST_CLIENT list) {
  int i;
  long size = list.size;
  for(i = 0; i < size; i++) {
    print_profile(list.elements[i]);
  }
}

/**
 * @brief prints the information of a client
 * @param client the client to print
 */
void print_profile(const CLIENT client) {
  printf("\nNIF: %lu\n", client.nif);
  printf("Nome: %s\n", client.name);
  printf("CC: %lu\n", client.cc);
  print_address(client.address);
  printf("Estado: %s\n", client.active ? "Ativo" : "Desativado");
}

/**
 * @brief private method that asks for the information of a client
 * @param nif the nif of the client
 * @return the information of the client
 */
CLIENT read_client(const unsigned long int nif) {
  CLIENT client;
  client.nif = nif;
  client.active = 1;
  client.has_orders = 0;
  change_client_name(client.name);
  change_cc(&client.cc);
  client.address = get_address();
  return client;
}

/**
 * @brief check if a certain client exists
 * @param list the list of clients
 * @param nif the nif to check if exists
 * @return -1 if client doesn't exist or the position of the client if this exists
 */
long int client_exists(const LIST_CLIENT list, const unsigned long int nif) {
  int i;
  long size = list.size;
  for(i = 0; i < size; i++) {
    if (list.elements[i].nif == nif)
      return i;
  }
  return -1;
}

/**
 * @brief checks if the client is active
 * @param list a list of clients
 * @param pos the position of the client
 * @return if is active 1 if not 0
 */
int client_is_active(const CLIENT *list, const unsigned long int pos) {
  return list[pos].active;
}

/**
 * @brief changed the name of the user
 * @param name var where to store the name
 */
void change_client_name(char *name) {
  read_string(name, NORMAL_STRING + 1, "Introduza o seu nome: ");
}

/**
 * @brief change the cc of the user
 * @param cc var where to store the cc
 */
void change_cc(unsigned long int *cc) {
  read_unsigned_long(cc, 10000000, 99999999, "Introduza o seu CC: ");
}

/**
 * @brief a function that activates or deactivates a user
 * @param state the state of the user 1, 0 (activated, deactivated)
 */
void change_active_state(unsigned short int *state) {
  if (*state == 1)
    *state = 0;
  else
    *state = 1;
}
