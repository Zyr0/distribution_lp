/*
 * File:   menus.c
 *
 * Created on December 19, 2019, 9:56 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include "../includes/api_leitura.h"
#include "../includes/menus.h"
#include "../includes/client.h"
#include "../includes/order.h"
#include "../includes/item.h"
#include "../includes/price.h"
#include "../includes/invoice.h"
#include "../includes/listings.h"

/**
 * @brief a menu for the client
 * @param list a list of clients
 * @param table the table with all the prices
 */
void menu_client(LIST_CLIENT *list, PRICE_TABLE *table) {
  unsigned short int op, month;
  unsigned long int nif;
  long int pos;

  read_unsigned_long(&nif, 100000000, 999999999, "Introduza o NIF: ");
  pos = client_exists(*list, nif);
  if (pos != -1) {
    if (client_is_active(list->elements, pos)) {
      do {
        printf("\n###################\n");
        printf("# Menu Utilizador #\n");
        printf("###################\n");
        printf("1 - Gerir conta\n");
        printf("2 - Gerir encomendas\n");
        printf("3 - Consultar tabela preços\n");
        printf("4 - Consultar Faturas\n");
        printf("0 - Voltar atrás\n");
        read_unsigned_short(&op, 0, 4, "Introduza uma opção: ");

        switch (op) {
          case 1:
            menu_client_manage_acount(&list->elements[pos]);
            break;
          case 2:
            if ((*list).elements[pos].has_orders == 0) {
              init_list_order(&list->elements[pos].orders, 1);
              (*list).elements[pos].has_orders = 1;
            }
            menu_order_client_manage(&list->elements[pos], table);
            break;
          case 3:
            menu_show_prices(table);
            break;
          case 4:
            printf("\n");
            if ((*list).elements[pos].has_orders == 1) {
              read_unsigned_short(&month, 1, 12, "Introduza um mês: ");
              print_invoice((*list).elements[pos].orders, month);
            } else {
              printf("Sem encomendas\n");
            }
            break;
        }
      } while (op != 0);

    } else {
      printf("\nA conta não esta ativa peça ao diretor para reativar a conta ...\n");
    }
  } else {
    printf("\nDeseja criar novo utilizador?\n1 - Sim\n0 - Não\n");
    read_unsigned_short(&op, 0, 1, "Introduza uma opção: ");

    if (op == 1) {
      putchar('\n');
      client_create(list, read_client(nif));
      printf("\nCliente criado com sucesso\n");
    }
  }
  printf("\nA voltar para menu principal ...\n");
}

/**
 * @brief a menu for the director
 * @param list the list of clients
 * @param table the table of prices
 */
void menu_director(LIST_CLIENT *list, PRICE_TABLE *table) {
  unsigned short int op;

  do {
    printf("\n##################\n");
    printf("#  Menu Diretor  #\n");
    printf("##################\n");
    printf("1 - Gerir clientes\n");
    printf("2 - Gerir encomendas\n");
    printf("3 - Gerir preços\n");
    printf("4 - Listagens\n");
    printf("0 - Voltar atrás\n");
    read_unsigned_short(&op, 0, 4, "Introduza uma opção: ");

    switch (op) {
      case 1:
        if ((*list).size > 0)
          menu_manage_clients(list);
        else
          printf("\nSem clientes\n");
        break;
      case 2:
        menu_manage_orders(list);
        break;
      case 3:
        menu_prices(table);
        break;
      case 4:
        menu_listings(*list);
        break;
    }
  } while (op != 0);

  printf("\nA voltar para menu principal ...\n");
}

/**
 * @brief a menu for listings
 * @param list the client list
 */
void menu_listings(const LIST_CLIENT list) {
  unsigned short int op, month;

  do {
    printf("\n###############\n");
    printf("#  Listagens  #\n");
    printf("###############\n");
    printf("1 - Quantas encomendas expedidas num mês\n");
    printf("2 - Encomenda mais leve do mês\n");
    printf("3 - Encomenda mais cara do mês\n");
    printf("4 - Cliente com mais encomendas do mês\n");
    printf("5 - Cliente com maior gasto do mês\n");
    printf("0 - Voltar atrás\n");
    read_unsigned_short(&op, 0, 5, "Introduza uma opção: ");

    if (list.size > 0 && op != 0) {
      read_unsigned_short(&month, 1, 12, "\nIntroduza um mês: ");
      switch (op) {
        case 1:
          printf("Total de encomendas expedidas no mês %hu foram %lu\n",
              month, order_expedited_in_month(list, month));
          break;
        case 2:
          printf("A encomenda mais leve do mês é: \n");
          print_tuple(order_light_in_month(list, month));
          break;
        case 3:
          printf("A encomenda mais cara do mês é: \n");
          print_tuple(order_expensive_in_month(list, month));
          break;
        case 4:
          printf("O cliente com mais encomendas no mês %hu é: ", month);
          print_profile(most_orders_in_month(list, month));
          break;
        case 5:
          printf("O cliente com maior gasto no mês %hu é: ", month);
          print_profile(most_spent_in_month(list, month));
          break;
      }
    } else {
      printf("Sem clientes\n");
    }
  } while(op != 0);
}

/**
 * @brief a submenu to manage the clients
 * @param list the list of clients
 */
void menu_manage_clients(LIST_CLIENT *list) {
  unsigned short int op;
  unsigned long int nif;
  long int pos;

  do {
    printf("\n####################\n");
    printf("#  Gerir Clientes  #\n");
    printf("####################\n");
    printf("1 - Listar Clients\n");
    printf("2 - Ativar/Desativar Client\n");
    printf("0 - Voltar atrás\n");
    read_unsigned_short(&op, 0, 2, "Introduza uma opção: ");

    putchar('\n');
    switch (op) {
      case 1:
        list_clients(*list);
        break;
      case 2:
        read_unsigned_long(&nif, 100000000, 999999999, "Introduza o NIF: ");
        pos = client_exists(*list, nif);
        if (pos != -1) {
          change_active_state(&(*list).elements[pos].active);
          if ((*list).elements[pos].active == 1)
            printf("Client ativado\n");
          else
            printf("Client desativado\n");
        } else {
          printf("Cliente não existe\n");
        }
        break;
    }
  } while (op != 0);
}

/**
 * @brief a submenu to manage the orders
 * @param list the list of clients
 */
void menu_manage_orders(LIST_CLIENT *list) {
  unsigned short int op;
  unsigned long int nif, order_id;
  long int pos, order_pos;

  do {
    printf("\n######################\n");
    printf("#  Gerir Encomendas  #\n");
    printf("######################\n");
    printf("1 - Listar Encomendas\n");
    printf("2 - Mudar estado encomenda\n");
    printf("0 - Voltar atrás\n");
    read_unsigned_short(&op, 0, 2, "Introduza uma opção: ");

    if (op != 0) {
      read_unsigned_long(&nif, 100000000, 999999999, "Introduza o NIF do cliente: ");
      pos = client_exists(*list, nif);
    }
    putchar('\n');
    if (pos != -1) {
      switch (op) {
        case 1:
          list_orders((*list).elements[pos].orders);
          break;
        case 2:
          read_unsigned_long(&order_id, 0, (*list).elements[pos].orders.id_count, "Introduza id encomenda: ");
          order_pos = order_exists((*list).elements[pos].orders, order_id);
          if (order_pos != -1) {
            change_order_state(&(*list).elements[pos].orders.elements[order_pos]);
          } else {
            printf("Encomenda não existe\n");
          }
          break;
      }
    } else {
      printf("Cliente não existe\n");
    }
  } while (op != 0);
}

/**
 * @brief a submenu to manage the orders
 * @param client the client that will manage the order
 * @param table the table with the prices
 */
void menu_order_client_manage(CLIENT *client, PRICE_TABLE *table) {
  unsigned short int op, sub_op;
  unsigned long int id;
  long int pos;
  LIST_ITEM items;
  ORDER order;

  do {
    printf("\n######################\n");
    printf("#  Gerir Encomendas  #\n");
    printf("######################\n");
    printf("1 - Listar encomendas\n");
    printf("2 - Criar encomenda\n");
    printf("3 - Editar encomenda\n");
    printf("4 - Eliminar encomenda\n");
    printf("0 - Voltar atrás\n");
    read_unsigned_short(&op, 0, 4, "Introduza uma opção: ");

    if (op == 3 || op == 4) {
      read_unsigned_long(&id, 0, (*client).orders.id_count, "Introduza o id da encomenda: ");
      pos = order_exists((*client).orders, id);
    }

    switch (op) {
      case 1:
        if ((*client).orders.size > 0)
          list_orders((*client).orders);
        else
          printf("\nSem encomendas\n");
        break;
      case 2:
        init_list_item(&items, 1);
        menu_item_manage(&items);
        if (items.size > 0) {
          printf("########################\n");
          printf("#  Detalhes encomenda  #\n");
          printf("########################\n");
          order = get_order(&items, (*client).address, (*client).orders.id_count);
          calc_price(&order, table);
          order_create(&(*client).orders, order);
        } else {
          printf("Sem items\n");
        }
        break;
      case 3:
        if ((*client).orders.size > 0)
          menu_order_client_edit(&(*client).orders, pos, table);
        else
          printf("\nSem encomendas\n");
        break;
      case 4:
        if ((*client).orders.size > 0) {
          printf("\nTem a certeza que quer eliminar a encomenda?\n1 - Sim\n0 - Não\n");
          read_unsigned_short(&sub_op, 0, 1, "Introduza uma opção: ");
          if (sub_op == 1)
            order_remove(&(*client).orders, pos);
        } else {
          printf("\nSem encomendas\n");
        }
        break;
    }
  } while (op != 0);
}

/**
 * @brief a submenu for editing items
 * @param list a list of orders
 * @param pos the position of the order
 * @param table the table of prices
 */
void menu_order_client_edit(LIST_ORDER *list, const long int pos, PRICE_TABLE *table) {
  unsigned short int op;

  do {
    printf("\n######################\n");
    printf("#  Editar encomenda  #\n");
    printf("######################\n");
    printf("1 - Gerir artigos\n");
    printf("2 - Mudar Origem\n");
    printf("3 - Mudar Destino\n");
    printf("4 - Mudar km\n");
    printf("5 - Mudar tipo encomenda\n");
    printf("0 - Voltar atrás\n");
    read_unsigned_short(&op, 0, 6, "Introduza uma opção: ");

    if (pos != -1) {
      if (is_expedited(list->elements, pos) == 0) {
        switch(op) {
          case 1:
            menu_item_manage(&list->elements[pos].items);
            break;
          case 2:
            change_address(&list->elements[pos].start);
            break;
          case 3:
            change_address(&list->elements[pos].end);
            break;
          case 4:
            change_order_km(&list->elements[pos].km);
            break;
        }
        if (op != 0) {
            free(list->elements[pos].order_type.elements);
            init_list_order_type(&list->elements[pos].order_type, 1);
            change_order_type_list(&list->elements[pos].items, &list->elements[pos].order_type);
        }
        calc_price(&list->elements[pos], table);
      } else {
        printf("Encomenda já expedida\n");
      }
    } else {
      printf("Encomenda não existe\n");
    }
  } while(op != 0);
}

/**
 * @brief a submenu for the creation of items
 * @param items the list of items
 */
void menu_item_manage(LIST_ITEM *items) {
  unsigned short int op, sub_op;
  long pos;
  ITEM item;
  char name[NORMAL_STRING + 1];

  do {
    printf("\n###################\n");
    printf("#  Gerir Artigos  #\n");
    printf("###################\n");
    printf("1 - Listar artigos\n");
    printf("2 - Adicionar artigo\n");
    printf("3 - Editar artigo\n");
    printf("4 - Remover artigo\n");
    printf("0 - Voltar atrás\n");
    read_unsigned_short(&op, 0, 4, "Introduza uma opção: ");

    putchar('\n');
    if (op != 0 && op != 1 && (items->size > 0 || op == 2)) {
      change_item_name(name);
      pos = item_exists(*items, name);
    }
    switch (op) {
      case 1:
        if (items->size > 0)
          list_items(*items);
        else
          printf("Sem artigos\n");
        break;
      case 2:
        if (pos >= 0) {
          printf("\nO artigo já existe\n");
        } else {
          item = get_item(name);
          printf("\nTem a certeza que quer adiciona o artigo?\n1 - Sim\n0 - Não\n");
          read_unsigned_short(&sub_op, 0, 1, "Introduza uma opção: ");
          if (sub_op == 1)
            item_create(items, item);
        }
        break;
      case 3:
        if (items->size > 0) {
          if (pos >= 0)
            menu_item_edit(&(*items).elements[pos]);
          else
            printf("\nArtigo não existe\n");
        }
        break;
      case 4:
        if (items->size > 0) {
          if (pos >= 0) {
            printf("\nTem a certeza que quer eliminar o artigo?\n1 - Sim\n0 - Não\n");
            read_unsigned_short(&sub_op, 0, 1, "Introduza uma opção: ");
            if (sub_op == 1)
              item_remove(items, pos);
          } else {
            printf("\nArtigo não existe\n");
          }
        }
        break;
      }
  } while (op != 0);
}

/**
 * @brief a submenu to manage the item
 * @param item a pointer for the item
 */
void menu_item_edit(ITEM *item) {
  unsigned short int op;

  do {
    printf("\n###################\n");
    printf("#  Editar Artigo  #\n");
    printf("###################\n");
    printf("1 - Editar Quantidade\n");
    printf("2 - Editar Peso\n");
    printf("3 - Editar Volume\n");
    printf("4 - Editar Frágil\n");
    printf("0 - Voltar atrás\n");
    read_unsigned_short(&op, 0, 4, "Introduza uma opção: ");

    putchar('\n');
    switch (op) {
      case 1:
        change_item_quantity(&item->quantity);
        break;
      case 2:
        change_item_weight(&item->weight);
        break;
      case 3:
        change_item_volume(&item->volume);
        break;
      case 4:
        change_item_fragile(&item->fragile);
        break;
    }
  } while (op != 0);
}

/**
 * @brief a submenu to manage the user
 * @param client a pointer for the client
 */
void menu_client_manage_acount(CLIENT *client) {
  unsigned short int op;

  do {
    printf("\n#################\n");
    printf("#  Gerir Conta  #\n");
    printf("#################\n");
    printf("1 - Consultar perfil\n");
    printf("2 - Editar perfil\n");
    printf("3 - Desativar conta\n");
    printf("0 - Voltar atrás\n");
    read_unsigned_short(&op, 0, 3, "Introduza uma opção: ");

    putchar('\n');
    switch (op) {
      case 1:
        print_profile(*client);
        break;
      case 2:
        menu_client_edit_profile(client);
        break;
      case 3:
        change_active_state(&(*client).active);
        if ((*client).active == 1)
          printf("Client ativado\n");
        else
          printf("Client desativado\n");
        break;
    }
  } while (op != 0);
}

/**
 * @brief a menu to handle the modification of the user
 * @param client a pointer to the client
 */
void menu_client_edit_profile(CLIENT *client) {
  unsigned short int op;

  do {
    printf("#####################\n");
    printf("#   Editar perfil   #\n");
    printf("#####################\n");
    printf("1 - Mudar nome\n");
    printf("2 - Mudar CC\n");
    printf("3 - Mudar morada\n");
    printf("0 - Voltar atrás\n");
    read_unsigned_short(&op, 0, 3, "Introduza uma opção: ");

    putchar('\n');
    switch (op) {
      case 1:
        change_client_name(client->name);
        break;
      case 2:
        change_cc(&client->cc);
        break;
      case 3:
        menu_address_edit(&client->address);
        break;
    }
  } while (op != 0);
}

/**
 * @brief a menu to handle the modification of the user address
 * @param address a pointer to the client address
 */
void menu_address_edit(ADDRESS *address) {
  unsigned short int op;

  do {
    printf("#####################\n");
    printf("#   Editar morada   #\n");
    printf("#####################\n");
    printf("1 - Mudar rua\n");
    printf("2 - Mudar localidade\n");
    printf("3 - Mudar distrito\n");
    printf("4 - Mudar código postal\n");
    printf("0 - Voltar atrás\n");
    read_unsigned_short(&op, 0 ,4, "Introduza uma opção: ");

    putchar('\n');
    switch (op) {
      case 1:
        change_street(address->street);
        break;
      case 2:
        change_local(address->local);
        break;
      case 3:
        change_district(address->district);
        break;
      case 4:
        change_zip(&address->zip_4, &address->zip_3);
        break;
    }
  } while (op != 0);
}

/**
 * @brief method that edit the price value for the user selected option type
 * @param table that contains the types price
 */
void menu_order_type_price(PRICE_TABLE * table) {
  unsigned short int op;

  do {
    printf("\n##############################\n");
    printf("#  Preço tipo de transporte  #\n");
    printf("##############################\n");
    printf("1 - Regular \n");
    printf("2 - Urgente \n");
    printf("3 - Volumoso \n");
    printf("4 - Frágil \n");
    printf("5 - Pesado \n");
    printf("0 - Voltar atrás\n");
    read_unsigned_short(&op, 0, 5, "Introduza uma opção: ");

    edit_order_type_prices(table, op);
  } while (op != 0);
}

/**
 * @brief menu with operations for cost and price management
 * @param table the table that contains the price definitions
 */
void menu_prices(PRICE_TABLE *table){
  unsigned short int op;

  do {
    printf("\n##################\n");
    printf("#  Gerir Preços  #\n");
    printf("##################\n");
    printf("1 - Definir Preços \n");
    printf("2 - Ver Preços \n");
    printf("0 - Voltar atrás\n");
    read_unsigned_short(&op, 0, 2, "Introduza uma opção: ");

    switch(op){
      case 1:
        menu_define_prices(table);
        break;
      case 2:
        menu_show_prices(table);
        break;
    }
  } while (op != 0);
}

/**
 * @brief Method that asks user which cost value he wants to see
 * @param table the table that contains the price definitions
 */
void menu_show_prices(PRICE_TABLE *table){
  unsigned short int op;

  do {
    printf("\n################\n");
    printf("#  Ver Preços  #\n");
    printf("################\n");
    printf("1 - Preço por quilometro \n");
    printf("2 - Fatores código postal \n");
    printf("3 - Tipo transporte \n");
    printf("0 - Voltar atrás\n");
    read_unsigned_short(&op, 0, 3, "Introduza uma opção: ");

    putchar('\n');
    switch(op){
      case 1:
        if (table->price_km != -1)
          show_km_price(*table);
        else
          printf(MSG_TABLE_NEDDED);
        break;
      case 2:
        if (table->cod_table_status == 1)
          show_postal_code_prices(*table);
        else
          printf(MSG_TABLE_NEDDED);
        break;
      case 3:
        if (table->order_type_table_status == 1)
          show_order_type_prices(*table);
        else
          printf(MSG_TABLE_NEDDED);
        break;
    }
  } while (op != 0);
}

/**
 * @brief method that permit to define cost values
 * @param table the table that contains the price definitions
 */
void menu_define_prices(PRICE_TABLE *table){
  unsigned short int op;

  do {
    printf("\n####################\n");
    printf("#  Definir Preços  #\n");
    printf("####################\n");
    printf("1 - Preço por quilometro\n");
    printf("2 - Tabela fator código Postal\n");
    printf("3 - Preços tipo transporte\n");
    printf("0 - Voltar atrás\n");
    read_unsigned_short(&op, 0, 4, "Introduza uma opção: ");

    putchar('\n');
    switch(op){
      case 1:
        define_km_price(table);
        break;
      case 2:
        if (table->cod_table_status == 1)
          edit_postal_code_prices(table);
        else
          create_postal_code_prices(table);
        break;
      case 3:
        if (table->order_type_table_status == 1)
          menu_order_type_price(table);
        else
          create_order_type_prices(table);
        break;
    }
  } while(op != 0);
}
