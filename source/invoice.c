/**
 * File:   invoice.c
 *
 * Created on January 10, 2020, 2:08 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include "../includes/api_leitura.h"
#include "../includes/invoice.h"
#include "../includes/client.h"
#include "../includes/order.h"
#include "../includes/price.h"
#include "../includes/item.h"
#include "../includes/date.h"

/**
 * @brief a method that prints an invoice of a month
 * @param orders the orders to print the invoice from
 * @param month the month of that invoice
 */
void print_invoice(const LIST_ORDER orders, unsigned short int month) {
  long int i, size_orders = orders.size;
  float total_price = 0;

  for (i = 0; i < size_orders; i++) {
    if (is_expedited(orders.elements, i) == 1 && orders.elements[i].date_ex.month == month) {
      total_price += orders.elements[i].price;

      printf("\nEncomenda %ld | Expedida em ", orders.elements[i].id);
      print_date(orders.elements[i].date_ex);
      printf("\n");

      list_items(orders.elements[i].items);

      printf("Contagem artigos: %ld\n", orders.elements[i].items.size);
      printf("Preço: %f\n", orders.elements[i].price);
    }
    printf("\n");
  }

  if (total_price > 0) {
    printf("Preço Total: %f\n", total_price);
  } else {
    printf("Não existem encomendas expedidas em %hu", month);
  }
}
