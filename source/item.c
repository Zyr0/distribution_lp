/**
 * File:   item.c
 *
 * Created on December 22, 2019, 2:37 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../includes/api_leitura.h"
#include "../includes/consts.h"
#include "../includes/item.h"

/**
 * @brief initialize the list of items @warning if the allocation fails the program exits
 * with code -1
 * @param list receives a LIST_ITEM
 * @param init_size the initial size of the list
 */
void init_list_item(LIST_ITEM *list, size_t init_size) {
  list->elements = (ITEM *) malloc(init_size * sizeof(ITEM));
  if (list->elements == NULL) {
    fprintf(stderr, "Erro ao alocar memória\n");
    exit(-1);
  }
  list->length = init_size;
  list->size = 0;
}

/**
 * @brief a method adds a new item to the list @warning if the list cannot be
 * reallocated prints a error message and exits with code -1
 * @param list the list of items
 * @param item the item to add to the list
 * @return the current size of the list
 */
size_t item_create(LIST_ITEM *list, const ITEM item) {
  if (list->size == list->length) {
    list->length += 1;
    list->elements = (ITEM *) realloc(list->elements, list->length * sizeof(ITEM));
    if (list->elements == NULL) {
      fprintf(stderr, "Erro ao realocar memória\n");
      exit(-1);
    }
  }
  list->elements[list->size++] = item;
  return list->size;
}

/**
 * @brief a method that removes a item from the list @warning if the list cannot be
 * reallocated prints a error message and exits with code -1
 * @param list the list of items
 * @param pos the position to remove
 * @return the current size of the list
 */
size_t item_remove(LIST_ITEM *list, const size_t pos) {
  list->elements[pos] = list->elements[list->size];
  list->size--;
  list->length -= 1;
  list->elements = (ITEM *) realloc(list->elements, list->length * sizeof(ITEM));
  if (list->elements == NULL) {
    fprintf(stderr, "Erro ao realocar memória\n");
    exit(-1);
  }
  return list->size;
}

/**
 * @brief a methods that checks if a item exists
 * @param list the list with the elements
 * @param name the name of the item
 * @return the position of the element
 */
long int item_exists(const LIST_ITEM list, const char *name) {
  long int i, size = list.size;
  for(i = 0; i < size; i++) {
    if (strcmp(list.elements[i].name, name) == 0)
      return i;
  }
  return -1;
}

/**
 * @brief a methods that lists all of the items
 * @param list the list to print
 */
void list_items(const LIST_ITEM list) {
  long int i, size = list.size;
  for(i = 0; i < size; i++) {
    list_item(list.elements[i]);
    printf("\n");
  }
}

/**
 * @brief a method that prints a item
 * @param item the item to print
 */
void list_item(const ITEM item) {
  printf("Nome: %s\n", item.name);
  printf("Quantidade: %u\n", item.quantity);
  printf("Peso por artigo: %.2f kg\n", item.weight);
  printf("Peso Total: %.2f kg\n", item.weight * item.quantity);
  printf("Volume por artigo: %.3f m^3\n", item.volume);
  printf("Volume Total: %.3f m^3\n", item.volume * item.quantity);
  printf("Frágil: %s\n", item.fragile == 1 ? "Sim" : "Não");
}

/**
 * @brief method that asks for the information of an item
 * @param name the name of the item
 * @return the item imputed for the user
 */
ITEM get_item(const char *name) {
  ITEM item;
  strcpy(item.name, name);
  change_item_quantity(&item.quantity);
  change_item_weight(&item.weight);
  change_item_volume(&item.volume);
  change_item_fragile(&item.fragile);
  return item;
}

/**
 * @brief a function that asks for a name to be imputed
 * @param s the pointer where the name will be stored
 */
void change_item_name(char *s) {
  read_string(s, NORMAL_STRING + 1, "Introduza o nome do artigo: " );
}

/**
 * @brief a function that asks for a quantity to be imputed @warning the max quantity is 50000
 * @param quantity the pointer where the quantity will be stored
 */
void change_item_quantity(unsigned int *quantity) {
  read_unsigned_int(quantity, 1, MAX_ITEM_QUANTITY, "Introduza a quantidade: ");
}

/**
 * @brief a function that asks for the weight of the item @warning the weight does not factor the
 * quantity, the max weight is 100kg
 * @param weight the pointer where the weight will be stored
 */
void change_item_weight(float *weight) {
  read_float(weight, 0.0, MAX_ITEM_WEIGHT, "Introduza o peso do artigo em kg: ");
}

/**
 * @brief a function that asks for the volume of the item @warning convertes from cm^3 to m^3 also
 * does not factor the quantity, the max length, width and height is 2000cm
 * @param volume the pointer where the volume will be stored
 */
void change_item_volume(float *volume) {
  float length, width, height;
  read_float(&length, 0, MAX_ITEM_LENGTH, "Introduza o comprimento em cm: ");
  read_float(&width, 0, MAX_ITEM_WIDTH, "Introduza a largura em cm: ");
  read_float(&height, 0, MAX_ITEM_HEIGHT, "Introduza a altura em cm: ");
  *volume = (length * width * height) * 0.000001;
}

/**
 * @brief a function that asks if the item is fragile or not
 * @param fragile the pointer where the state of fragile will be stored
 */
void change_item_fragile(unsigned short int *fragile) {
  printf("O artigo é frágil?\n1 - Sim\n0 - Não\n");
  read_unsigned_short(fragile, 0, 1, "Introduza uma opção: ");
}

/**
 * @brief a function that counts the total weight of the items on the order
 * @param list the list of items
 */
float get_total_weight(const LIST_ITEM list) {
  float weight_count = 0;
  long int i, size = list.size;
  for (i = 0; i < size; i++)
    weight_count += list.elements[i].weight * list.elements[i].quantity;
  return weight_count;
}

/**
 * @brief a function that counts the total volume of the items on the order
 * @param list the list of items
 */
float get_total_volume(const LIST_ITEM list) {
  float volume_count = 0;
  long i, size = list.size;
  for (i = 0; i < size; i++)
    volume_count += list.elements[i].volume * list.elements[i].quantity;
  return volume_count;
}

/**
 * @brief a function that counts the total items considered fragile in a list
 * @param list the list of items
 * @return the count of items considered fragile
 */
long int get_total_fragile(const LIST_ITEM list) {
  long int i, count = 0, size = list.size;
  for (i = 0; i < size; i++) {
    if (list.elements[i].fragile == 1)
      count++;
  }
  return count;
}
