/**
 * File:   price.c
 *
 * Created on 9 de janeiro de 2020, 09:24
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../includes/api_leitura.h"
#include "../includes/consts.h"
#include "../includes/price.h"
#include "../includes/order.h"

/**
 * @brief initiates the status of the price table
 * @param table the table to be initiated
 */
void init_price_table(PRICE_TABLE *table) {
  table->order_type_table_status = 0;
  table->cod_table_status = 0;
  table->price_km = -1;
}

/**
 * @brief a method that checks if the table has information
 * @param table the table to check
 */
unsigned short int is_price_defined(const PRICE_TABLE table) {
  if (table.order_type_table_status == 1 &&
      table.cod_table_status == 1 && table.price_km != 0)
    return 1;
  return 0;
}

/**
 * @brief method that asks for the factor number for each postal code
 * @warning the positions like 2 3 will have the same value as 3 2
 * @param table the pointer where the factor will be stored
 */
void create_postal_code_prices(PRICE_TABLE *table) {
  int i, j;
  float val;
  for (i = 0; i < MAX_SIZE_POSTAL_CODE; i++) {
    for (j = i; j < MAX_SIZE_POSTAL_CODE; j++) {
      printf("Introduza fator para %d %d: ", i + 1, j + 1);
      read_float(&val, 0, MAX_FACTOR_POSTAL_CODE, " ");
      (*table).price_postal_code[i][j] = val;
      (*table).price_postal_code[j][i] = val;
    }
  }
  table->cod_table_status=1;
}

/**
 * @brief method that create the price table with all order types
 * @param table that will have the price for each type
 */
void create_order_type_prices(PRICE_TABLE *table) {
  float value;
  read_float(&value, 1, MAX_PRICE_ORDER_TYPE, "Introduza preço para regular: ");
  (*table).regular = value;
  read_float(&value, 1, MAX_PRICE_ORDER_TYPE, "Introduza preço para urgente: ");
  (*table).urgent = value;
  read_float(&value, 1, MAX_PRICE_ORDER_TYPE, "Introduza preço para volumoso: ");
  (*table).bulky = value;
  read_float(&value, 1, MAX_PRICE_ORDER_TYPE, "Introduza preço para frágil: ");
  (*table).fragile = value;
  read_float(&value, 1, MAX_PRICE_ORDER_TYPE, "Introduza preço para pesado: ");
  (*table).heavy = value;
  table->order_type_table_status=1;
}

/**
 * @brief method that ask for line and column and the value to be changed on the table
 * @warning the positions like 2 3 will have the same value as 3 2
 * @param table target table to be edited
 */
void edit_postal_code_prices(PRICE_TABLE *table) {
  unsigned int l, c;
  float value;
  printf("Qual a Linha e Coluna da tabela que deseja alterar?\n");
  read_unsigned_int(&l, 1, MAX_SIZE_POSTAL_CODE, "Linha: ");
  read_unsigned_int(&c, 1, MAX_SIZE_POSTAL_CODE, "Coluna: ");
  read_float(&value, 0, MAX_FACTOR_POSTAL_CODE, "Novo fator: ");
  (*table).price_postal_code[l - 1][c - 1] = value;
  (*table).price_postal_code[c - 1][l - 1] = value;
}

/**
 * @brief method that edit the price value for the user selected option type
 * @param table that contains the types price
 * @param op the options that the director chouse
 */
void edit_order_type_prices(PRICE_TABLE *table, const unsigned short int op) {
  float value;

  if (op != 0) {
    putchar('\n');
    read_float(&value, 1, MAX_PRICE_ORDER_TYPE, "Novo preço: ");
  }

  switch (op) {
    case 1:
      (*table).regular = value;
      break;
    case 2:
      (*table).urgent = value;
      break;
    case 3:
      (*table).bulky = value;
      break;
    case 4:
      (*table).fragile = value;
      break;
    case 5:
      (*table).heavy = value;
      break;
  }
}

/**
 * @brief method that prints the table
 * @param table the table that will be printed
 */
void show_postal_code_prices(const PRICE_TABLE table) {
  int i, j;
  for (i = 0; i < MAX_SIZE_POSTAL_CODE; i++) {
    printf("%d", i+1);
    for (j = 0; j < MAX_SIZE_POSTAL_CODE; j++) {
      printf(" | %.2f", table.price_postal_code[i][j]);
    }
    printf("\n");
  }
}

/**
 * @brief method that prints the order type prices
 * @param table that contains the prices
 */
void show_order_type_prices(const PRICE_TABLE table) {
  printf("Preço regular: %.2f\n", table.regular);
  printf("Preço urgente: %.2f\n", table.urgent);
  printf("Preço volumoso: %.2f\n", table.bulky);
  printf("Preço frágil: %.2f\n", table.fragile);
  printf("Preço pesado: %.2f\n", table.heavy);
}

/**
 * @brief method that define the cost for km
 * @param table that contains the cost value for km
 */
void define_km_price(PRICE_TABLE *table) {
  float val;
  read_float(&val, 0.1, MAX_PRICE_KM, "Introduza preço por quilómetro: ");
  (*table).price_km = val;
}

/**
 * @brief a method that prints the cost value for km
 * @param table the table that contains the cost value for km
 */
void show_km_price(const PRICE_TABLE table) {
  printf("Preço de custo por quilometro é %.2f\n", table.price_km);
}

/**
 * @brief calculates the price of the order
 * @param order the order to have the price calculated
 * @param table the table that contains the prices
 */
void calc_price(ORDER *order, PRICE_TABLE *table) {
  long i, size = order->order_type.size;
  unsigned short int origin_pos = order->start.zip_4 / 1000,
      dest_pos = order->end.zip_4 / 1000;
  float type_value = 0, km_value = order->km * table->price_km;

  for (i = 0; i < size; i++) {
    switch (order->order_type.elements[i].type) {
      case REGULAR:
        type_value += table->regular;
        break;
      case URGENT:
        type_value += table->urgent;
        break;
      case BULKY:
        type_value += table->bulky;
        break;
      case FRAGILE:
        type_value += table->fragile;
        break;
      case HEAVY:
        type_value += table->heavy;
        break;
    }
  }
  (*order).price = (type_value + km_value) * table->price_postal_code[origin_pos][dest_pos];
}
