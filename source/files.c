/**
 * File:   files.c
 *
 * Created on January 11, 2020, 10:15 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include "../includes/files.h"
#include "../includes/price.h"
#include "../includes/client.h"
#include "../includes/order.h"

/**
 * @brief a method that lets me save to a file the clients and price table
 * @param clients where the clients to be saved
 * @param table where the table to be saved
 */
void save_to_file(LIST_CLIENT *clients, PRICE_TABLE *table) {
  FILE *table_file = NULL;
  FILE *clients_file = NULL;
  save_price_table(table, table_file);
  save_clients(clients, clients_file);
}

/**
 * @brief a method that lets me read from a file the clients and price table
 * @param clients where the clients will be saved
 * @param table where the table will be saved
 * @param max the max size of clients
 */
void read_from_file(LIST_CLIENT *clients, PRICE_TABLE *table, size_t max) {
  FILE *table_file = NULL;
  FILE *clients_file = NULL;
  read_price_table(table, table_file);
  read_clients(clients, clients_file, max);
}

/**
 * @brief a method that saves the price table
 * @param table the table of prices to save
 * @param file the file where the table will be saved to
 */
void save_price_table(PRICE_TABLE *table, FILE *file) {
  int flag = -1;
  file = fopen("price_table", "w");

  if (file == NULL) {
    fprintf(stderr, "\nErro a abrir ficheiro preço\n");
  } else {
    flag = fwrite(table, sizeof(PRICE_TABLE), 1, file);
    if (flag != -1)
      printf("\nTabela preços guardada\n");
    else
      fprintf(stderr, "\nErro ao guardar no ficheiro\n");
    fclose(file);
  }
}

/**
 * @brief a method that saves the clients
 * @param clients the clients to save
 * @param file the file where the table will be saved to
 */
void save_clients(LIST_CLIENT *clients, FILE *file) {
  int res = -1;
  size_t i;
  file = fopen("clients", "w");

  if (file == NULL) {
    fprintf(stderr, "\nErro a abrir ficheiro clients\n");
  } else {
    res = fwrite(&clients->size, sizeof(size_t), 1, file);
    if (res != -1) {
      res = -1;
      res = fwrite(clients->elements, sizeof(CLIENT), clients->size, file);
      if (res != -1) {
        for (i = 0; i < clients->size; i++) {
          if (clients->elements[i].has_orders == 1) {
            save_orders(&clients->elements[i].orders, file);
          }
        }
        printf("\nClients gravados\n");
      } else {
        fprintf(stderr, "\nErro ao gravar clients\n");
      }
    } else {
      fprintf(stderr, "\nErro ao gravar tamanho lista clients\n");
    }
    fclose(file);
  }
}

/**
 * @brief a method that saves all of the orders
 * @param orders the list of orders to be saved
 * @param file the file where the orders will be saved
 */
void save_orders(LIST_ORDER *orders, FILE *file) {
  int res = -1;
  size_t i;

  if (file == NULL) {
    fprintf(stderr, "\nErro a abrir ficheiro clients\n");
  } else {
    res = fwrite(&orders->size, sizeof(size_t), 1, file);
    if (res != -1) {
      res = -1;
      res = fwrite(&orders->id_count, sizeof(unsigned long int), 1, file);
      if (res != -1) {
        res = -1;
        res = fwrite(orders->elements, sizeof(ORDER), orders->size, file);
        if (res != -1) {
          for (i = 0; i < orders->size; i++) {
            save_items(&orders->elements[i].items, file);
            save_order_type(&orders->elements[i].order_type, file);
          }
          printf("Encomendas gravada\n");
        } else {
          fprintf(stderr, "\nErro ao gravar encomendas\n");
        }
      } else {
        fprintf(stderr, "\nErro ao gravar contagem id da lista encomendas\n");
      }
    } else {
      fprintf(stderr, "\nErro ao gravar tamanho lista encomendas\n");
    }
  }
}

/**
 * @brief a method that saves all of the items
 * @param items the list of items to be saved
 * @param file the file where the items will be saved
 */
void save_items(LIST_ITEM *items, FILE *file) {
  int res = -1;

  if (file == NULL) {
    fprintf(stderr, "\nErro a abrir ficheiro clients\n");
  } else {
    res = fwrite(&items->size, sizeof(size_t), 1, file);
    if (res != -1) {
      res = -1;
      res = fwrite(items->elements, sizeof(ITEM), items->size, file);
      if (res != -1) {
        printf("Artigos gravados\n");
      } else {
        fprintf(stderr, "\nErro ao gravar artigos\n");
      }
    } else {
      fprintf(stderr, "\nErro ao gravar contagem artigos\n");
    }
  }
}

/**
 * @brief a method that saves all of the order_types
 * @param order_type the list of order_types to be saved
 * @param file the file where the order_types will be saved
 */
void save_order_type(LIST_ORDER_TYPE *order_type, FILE *file) {
  int res = -1;

  if (file == NULL) {
    fprintf(stderr, "\nErro a abrir ficheiro clients\n");
  } else {
    res = fwrite(&order_type->size, sizeof(size_t), 1, file);
    if (res != -1) {
      res = -1;
      res = fwrite(order_type->elements, sizeof(ORDER_TYPE), order_type->size, file);
      if (res != -1) {
        printf("Tipos encomenda gravados\n");
      } else {
        fprintf(stderr, "\nErro ao gravar tipo de encomenda\n");
      }
    } else {
      fprintf(stderr, "\nErro ao gravar contagem tipo de encomenda\n");
    }
  }
}

/**
 * @brief a method that reads the price table
 * @param table the table of prices to read
 * @param file the file where the table is stored
 */
void read_price_table(PRICE_TABLE *table, FILE *file) {
  int flag = -1;
  file = fopen("price_table", "r");

  if (file == NULL) {
    fprintf(stderr, "\nErro a abrir ficheiro preço\n");
  } else {
    flag = fread(table, sizeof(PRICE_TABLE), 1, file);
    if (flag != -1)
      printf("\nTabela de preços lida\n");
    else
      fprintf(stderr, "\nErro ao ler ficheiro\n");
    fclose(file);
  }
}

/**
 * @brief a method that reads the clients
 * @param clients the table of clients to read
 * @param file the file where the table are stored
 * @param max the max size of the clients list
 */
void read_clients(LIST_CLIENT *clients, FILE *file, size_t max) {
  int res = -1;
  size_t i;
  file = fopen("clients", "r");

  if (file == NULL) {
    fprintf(stderr, "\nErro a abrir ficheiro clients\n");
  } else {
    res = fread(&clients->size, sizeof(size_t), 1, file);
    if (res != -1) {
      res = -1;
      if (clients->size > max) {
        client_resize(clients);
      }
      res = fread(clients->elements, sizeof(CLIENT), clients->size, file);
      if (res != -1) {
        for (i = 0; i < clients->size; i++) {
          if (clients->elements[i].has_orders == 1) {
            read_orders(&clients->elements[i].orders, file);
          }
        }
        printf("\nClients lidos\n");
      } else {
        fprintf(stderr, "\nErro a ler clients\n");
      }
    }
    fclose(file);
  }
}

/**
 * @brief a method that reads the orders of a client
 * @param orders the list of orders
 * @param file the file where the orders are stored
 */
void read_orders(LIST_ORDER *orders, FILE *file) {
  int res = -1;
  size_t i;

  if (file == NULL) {
    fprintf(stderr, "\nErro a abrir ficheiro clients\n");
  } else {
    res = fread(&orders->size, sizeof(size_t), 1, file);
    if (res != -1) {
      res = -1;
      init_list_order(orders, orders->size);
      orders->size = orders->length;
      res = fread(&orders->id_count, sizeof(unsigned long int), 1, file);
      if (res != -1) {
        res = fread(orders->elements, sizeof(ORDER), orders->size, file);
        if (res != -1) {
          for (i = 0; i < orders->size; i++) {
            read_items(&orders->elements[i].items, file);
            read_order_type(&orders->elements[i].order_type, file);
          }
          printf("Encomendas lidas\n");
        } else {
          fprintf(stderr, "\nErro a ler encomendas\n");
        }
      } else {
        fprintf(stderr, "\nErro a ler contagem id das encomendas\n");
      }
    }
  }
}

/**
 * @brief a method that reads the items of a order
 * @param items the list of items
 * @param file the file where the items are stored
 */
void read_items(LIST_ITEM *items, FILE *file) {
  int res = -1;

  if (file == NULL) {
    fprintf(stderr, "\nErro a abrir ficheiro clients\n");
  } else {
    res = fread(&items->size, sizeof(size_t), 1, file);
    if (res != -1) {
      res = -1;
      init_list_item(items, items->size);
      items->size = items->length;
      res = fread(items->elements, sizeof(ITEM), items->size, file);
      if (res != -1) {
        printf("Artigos lidos\n");
      } else {
        fprintf(stderr, "\nErro a ler artigos\n");
      }
    }
  }
}

/**
 * @brief a method that reads the order_types of a order
 * @param order_type the list of order_types
 * @param file the file where the order_types are stored
 */
void read_order_type(LIST_ORDER_TYPE *order_type, FILE *file) {
  int res = -1;

  if (file == NULL) {
    fprintf(stderr, "\nErro a abrir ficheiro clients\n");
  } else {
    res = fread(&order_type->size, sizeof(size_t), 1, file);
    if (res != -1) {
      res = -1;
      init_list_order_type(order_type, order_type->size);
      order_type->size = order_type->length;
      res = fread(order_type->elements, sizeof(ORDER_TYPE), order_type->size, file);
      if (res != -1) {
        printf("Tipo de encomenda lidos\n");
      } else {
        fprintf(stderr, "\nErro a ler tipo de encomenda\n");
      }
    }
  }
}
