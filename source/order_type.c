/*
 * File:   order_type.c
 *
 * Created on January 8, 2020, 9:06 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include "../includes/api_leitura.h"
#include "../includes/consts.h"
#include "../includes/order_type.h"

/**
 * @brief initialize the list of order types @warning if the allocation fails the program exits
 * with code -1
 * @param list receives a LIST_ORDER_TYPE
 * @param init_size the initial size of the list
 */
void init_list_order_type(LIST_ORDER_TYPE *list, const size_t init_size) {
  list->elements = (ORDER_TYPE *) malloc(init_size * sizeof(ORDER_TYPE));
  if (list->elements == NULL) {
    fprintf(stderr, "Erro ao alocar memória\n");
    exit(-1);
  }
  list->length = init_size;
  list->size = 0;
}

/**
 * @brief a private method adds a new order type to the list @warning if the list cannot be
 * reallocated prints a error message and exits with code -1
 * @param list the list of order types
 * @param type the type to add to the list
 * @return the current size of the list
 */
size_t order_type_create(LIST_ORDER_TYPE *list, const ORDER_TYPE type) {
  if (list->size == list->length) {
    list->length += 1;
    list->elements = (ORDER_TYPE *) realloc(list->elements, list->length * sizeof(ORDER_TYPE));
    if (list->elements == NULL) {
      fprintf(stderr, "Erro ao realocar memória\n");
      exit(-1);
    }
  }
  list->elements[list->size++] = type;
  return list->size;
}

/**
 * @brief prints the details of a certain list
 * @param list the list to print
 */
void list_order_types(const LIST_ORDER_TYPE list) {
  int i;
  long size = list.size;
  for(i = 0; i < size; i++) {
    list_order_type(list.elements[i]);
  }
}

/**
 * @brief prints the details of a order type
 * @param order_type the order type to print
 */
void list_order_type(ORDER_TYPE order_type) {
  switch(order_type.type) {
    case REGULAR:
      printf("Regular\n");
      break;
    case URGENT:
      printf("Urgente\n");
      break;
    case BULKY:
      printf("Volumoso\n");
      break;
    case FRAGILE:
      printf("Frágil\n");
      break;
    case HEAVY:
      printf("Pesado\n");
      break;
  }
}
